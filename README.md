# 5050 Django Frontend

# System Requirements

## Ubuntu


```
sudo apt-get update && sudo apt-get install python3-dev python3-pip python3-venv build-essential sqlite3 -y
```
# Installation

```bash
git clone https://gitlab.com/PBSA/PeerplaysIO/50-50/5050-django-frontend.git

cd 5050-django-frontend
```
It is best practice to use python virtual environments. https://docs.python.org/3/tutorial/venv.html

```bash
python3 -m venv env
source env/bin/activate
```
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the required packages.

```bash
pip3 install -r requirements.txt
```

Run migrations:
```
python3 manage.py migrate
```
# Usage
## Running the development server
```
python3 manage.py runserver
```
