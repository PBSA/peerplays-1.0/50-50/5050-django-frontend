# Generated by Django 3.1.1 on 2020-10-29 17:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('raffles', '0007_auto_20201029_1415'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='price',
            field=models.DecimalField(blank=True, decimal_places=2, default=0, max_digits=100, null=True),
        ),
    ]
