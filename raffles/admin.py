from django.contrib import admin
from .models import Raffle, Organization, Beneficiary, Condition, TicketBundle, Order, Seller

'''
Add inline models and register them to raffles in the admin
'''

class ConditionInLine(admin.TabularInline):
    model = Condition

class TicketBundleInLine(admin.TabularInline):
    model = TicketBundle

class BeneficiaryInLine(admin.TabularInline):
    model = Beneficiary

class SellerInLine(admin.TabularInline):
    model = Seller

class RaffleAdmin(admin.ModelAdmin):
    inlines = [
        ConditionInLine,
        TicketBundleInLine,
        BeneficiaryInLine,
    ]

class OrganizationAdmin(admin.ModelAdmin):
    inlines = [
        SellerInLine,
    ]

'''
Register your models with the admin here
'''
admin.site.register(Raffle, RaffleAdmin)
admin.site.register(Organization, OrganizationAdmin)
admin.site.register(Order)
admin.site.register(Seller)