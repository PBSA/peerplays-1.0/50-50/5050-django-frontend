from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.contrib import messages
# from django.core.mail import send_mail

from .models import Raffle, TicketBundle, Beneficiary, Seller, Entry
from .forms import OrderForm

def orderform(request, raffle_slug):
    ''' Renders the appropriate order form or processes form submissions ''' 

    # get raffle using slug
    raffle = get_object_or_404(Raffle, slug=raffle_slug)

    # create new order form instance
    form = OrderForm(request.POST or None)

    if form.is_valid():

        order = form.save(commit=False)

        if order.payment_method == 'cash' and request.POST['seller_code']:
            try:
                order.seller = Seller.objects.get(authorization_code=request.POST['seller_code'])
                # print("AUTH_CODE: SUCCESS - {}".format(order.seller))
                # messages.success(request, 'success!!')
            except Seller.DoesNotExist:
                messages.error(request, 'Seller Authorization Code is invalid.')
        
        # process Stripe payment
        order.stripe_confirmation = 'TEST'

        order.save()

        # create new entries by incrementing the entry numbers and saving them in loop
        if Entry.objects.filter(raffle=order.raffle):
            last_entry_number = Entry.objects.filter(raffle=order.raffle).last().entry_number
        else:
            last_entry_number = 0

        for entry in range(order.bundle.quantity):
            last_entry_number = last_entry_number+1
            Entry(
                raffle = order.raffle,
                order = order,
                organization = order.raffle.organization,
                entry_number = last_entry_number,
            ).save()
        
        # send confirmation email containing raffle title, name, ticket number and entry numbers.
        # send_mail(
        #     'Title: {}'.format(form.cleaned_data['name']),
        #     form.cleaned_data['description'],
        #     '{title}, <{slug}>'.format(**form.cleaned_data),
        #     ['gian.pompilio@gmail.com']
        # )
    
        # forward user to confirmation / thank-you page
        # return HttpResponseRedirect('/raffle/thanks/')

    # otherwise, render form page with appropriate context and error messages
    template = 'raffles/form.html'
    context = {
        'form' : form,
        'raffle' : raffle,
        }

    return render(request, template, context)


def thanks(request):
    return render(request, 'raffles/thanks.html')
